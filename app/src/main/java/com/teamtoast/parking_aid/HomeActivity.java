package com.teamtoast.parking_aid;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.app.AlertDialog;

import com.team_toast.framework.FileIO.InternalFileImp;
import com.team_toast.framework.FileIO.SimpleText;
import com.team_toast.framework.Location.Location;
import com.team_toast.framework.AppManagement.Permissions;
import com.team_toast.framework.AppManagement.Popup;
import com.team_toast.framework.Sound.SoundResource;
import com.team_toast.framework.Location.AndroidGpsLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class HomeActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "<com.teamtoast.parking_aid.app>.message";
    public static final String EXTRA_INT = "<com.teamtoast.parking_aid.app>.message2";
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Check for permissions
        Permissions p = new Permissions();
        p.requestCoarsePermissions(this);
        p.requestFinePermissions(this);

        //Start new thread to check for notifications
        handler = new Handler();
        //update(this, 60000);  //Updates every 1 minute (60000 milliseconds)
        update(this, 10000);  //Updates every 1 minute (60000 milliseconds)
        Assets.cars = Assets.loadCarsFromFile(this);
        // if (Assets.cars == null||Assets.cars.isEmpty()) {
        //  Intent intent = new Intent(this, carList.class);
        //Go to CarList activity
        //  startActivity(intent);
        // }
    }

    @Override
    public void onStop() {
        super.onStop();
        Assets.saveCars(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        Intent intent = getIntent();

        //If no existing car list, create new one and display welcome message
        if (Assets.cars.size() == 0) {
            Assets.cars = new ArrayList<>();
            Popup p = new Popup();
            p.newPopup(this, "Welcome to Parking Aid!", "Please enter in the details of your car and parking allowance. Continue on to the next " +
                    "pages to view and track your cars!");
        } else if(!intent.hasExtra("newcar")){

            Intent newIntent = new Intent(this, carList.class);
            //Go to CarList activity
            startActivity(newIntent);
        }


    }

    public Car createCar(String regNumber, String time) {
        //Get ticket start date
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy  HH:mm:ss");
        Date timeOfTicket = new Date();

        //Get ticket expiry date
        int timer = Integer.parseInt(time.toString());
        final long HOUR = 3600 * 1000;
        Date timeOfTicketExpiry = new Date(timeOfTicket.getTime() + timer * HOUR);

        //Stamp Location
        Location location = new AndroidGpsLocation(this);
        location.initialiseLocationManager(this);
        location.updateToLastKnownLocation();

        //If location is null, display popup message and return
        if (location.getLatLng() == null) {
            Popup p = new Popup();
            p.newPopup(this, "Error: Cannot get your location.", "Please make sure you have signal to add the car.");
            return null;
        }

        return new Car(regNumber, timeOfTicket, timeOfTicketExpiry, location);
    }

    public void addToList(Car newCar) {
        Assets.cars.add(newCar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }


    public void submitReg(View view) {

        //Get text from fields
        EditText regNumber = (EditText) findViewById(R.id.editText); //store reg number as string
        EditText time = (EditText) findViewById(R.id.editText2);

        //Check if fields have been filled
        if (regNumber.getText().toString().equals("") || time.getText().toString().matches("")) {
            Popup p = new Popup();
            p.newPopup(this, "Input error.", "Please make sure you have filled in both fields.");
            return;
        }

        //Check if car already exists
        if (Assets.cars != null) {
            String duplicateCheck = regNumber.getText().toString().toUpperCase();
            for (Car car : Assets.cars) {
                if (car.getRegNumber().matches(duplicateCheck)) {
                    Popup p = new Popup();
                    p.newPopup(this, "Duplicate error.", "This car already exists.");
                    return;
                }
            }
        }


        //Create the car
        String reg = regNumber.getText().toString().toUpperCase();
        String timer = time.getText().toString();
        Car newCar = createCar(reg, timer);

        //If car is null
        if (newCar == null) {
            return;
        }

        Assets.cars.add(newCar);

        Intent intent = new Intent(this, carList.class);

        //Go to CarList activity
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void update(final Context context, final int interval) {
        final Runnable updateTask = new Runnable() {
            @Override
            public void run() {

                //Notification threshold set to 15 minutes
                float notificationThreshold = (1000 * 60 * 15);

                //Check if notification is needed
                for (int i = 0; i < Assets.cars.size(); i++) {

                    //If remaining time is less than 15 minutes
                    if (Assets.cars.get(i).getRemainingTicketTime() < notificationThreshold) {
                        Assets.cars.get(i).sendNotification15(context, i);
                    }

                    //If car has expired
                    if (Assets.cars.get(i).getRemainingTicketTime() < 0.0f) {
                        Assets.cars.get(i).sendNotification0(context, i);
                    }
                }
                handler.postDelayed(this, interval);
            }
        };

        handler.postDelayed(updateTask, 1000);
    }

}
