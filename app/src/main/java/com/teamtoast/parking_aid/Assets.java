package com.teamtoast.parking_aid;

import android.app.Activity;

import com.team_toast.framework.FileIO.InternalFileImp;
import com.team_toast.framework.FileIO.SimpleText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by hvy13jku on 16/12/2015.
 */
public class Assets {

    public static ArrayList<Car> cars;
    public static Activity activity;

    public ArrayList<Car> getCars(){
        return cars;
    }

    public static void saveCars(Activity activity) {
        JSONArray carsToSave = new JSONArray();
        for (Car c : Assets.cars) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("reg", c.getRegNumber());
                jsonObject.put("timeOfTicket", c.getTimeOfTicket().getTime());
                jsonObject.put("timeOfTicketExpiry", c.getTimeOfTicketExpiry().getTime());
                jsonObject.put("lat", c.getLocation().getLatLng().latitude);
                jsonObject.put("lng", c.getLocation().getLatLng().longitude);
                jsonObject.put("notify15", c.getNotify15());
                jsonObject.put("notify0", c.getNotify0());

            } catch (Exception e) {

            }
            carsToSave.put(jsonObject);
        }

        SimpleText test = new SimpleText(new InternalFileImp(activity));
        test.setBody(carsToSave.toString());
        test.writeToFile("cars.pa");
    }

    public static ArrayList<Car> loadCarsFromFile(Activity activity) {
        // Assets.cars = new ArrayList<>();
        ArrayList<Car> savedCars = new ArrayList<>();
        SimpleText st = new SimpleText(new InternalFileImp(activity));
        st.readFromFile("cars.pa");
        String jsonStr = st.formatOutput();

        try {
            JSONArray loadedCars = new JSONArray(jsonStr);
            for (int i = 0; i < loadedCars.length(); i++) {
                try {

                    String reg = loadedCars.getJSONObject(i).getString("reg");
                    long timeOfTicket = loadedCars.getJSONObject(i).getLong("timeOfTicket");
                    long timeOfTicketExpiry = loadedCars.getJSONObject(i).getLong("timeOfTicketExpiry");
                    double lat = loadedCars.getJSONObject(i).getDouble("lat");
                    double lng = loadedCars.getJSONObject(i).getDouble("lng");
                    boolean n0 = loadedCars.getJSONObject(i).getBoolean("notify0");
                    boolean n15 = loadedCars.getJSONObject(i).getBoolean("notify15");

                    Car cd = new Car(reg,timeOfTicket,timeOfTicketExpiry,lat,lng,activity,n0,n15);
                    savedCars.add(cd);
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        } catch (JSONException e) {
            System.out.println(e);
        }
        return savedCars;
    }
}
