package com.teamtoast.parking_aid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.team_toast.framework.Maps.AndroidMap;
import com.team_toast.framework.Location.Location;
import com.team_toast.framework.Location.AndroidGpsLocation;

public class carInfo extends FragmentActivity {

    private Car car;
    private AndroidMap gMap;
    private Location appLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_car_info);

        Intent intent = getIntent();
        int index = (int) intent.getSerializableExtra("carIndex");

        car = Assets.cars.get(index);

        final TextView reg = (TextView) findViewById(R.id.textView3);
        reg.setText(car.getRegNumber());

        ////////MAP STUFF//////////////////////////
        //Init map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        //gMap = new AndroidMap(mapFragment, ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap());
        gMap = AndroidMap.getInstance();
        gMap.initialise(mapFragment, ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap());
        gMap.setMapDefaultPosition(new LatLng(52.622354, 1.239476));

        gMap.getGoogleMap().getUiSettings().setMyLocationButtonEnabled(false);

        //Our current location
        appLocation = new AndroidGpsLocation(this);
        appLocation.initialiseLocationManager(this);

        //Draw map
        gMap.updateRoute(appLocation, car.getLocation(), 10000);

    }
}
