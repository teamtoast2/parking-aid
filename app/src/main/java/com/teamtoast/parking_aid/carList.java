package com.teamtoast.parking_aid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.team_toast.framework.FileIO.InternalFileImp;
import com.team_toast.framework.FileIO.SimpleText;
import com.team_toast.framework.Location.Location;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class carList extends AppCompatActivity {

    private ListView lv;
    //public ArrayList<Car> carsOnFile;


    public void onResume() {
        super.onResume();

        if(Assets.cars.size()==0){
            Intent intent = new Intent(this, HomeActivity.class);
            //Go to CarList activity
            startActivity(intent);
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);

        Assets.activity=this;

        System.out.println(Assets.cars);
        lv = (ListView) findViewById(android.R.id.list);

        // Get the message from the intent
        Intent intent = getIntent();

        MyCustomAdapter adapter = new MyCustomAdapter(this);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
                // When clicked, show a toast with the TextView text
                goToCarInfo(position);
            }
        });
    }


    public void goToCarInfo(int index) {
        Intent intent = new Intent(this, carInfo.class);
        intent.putExtra("carIndex", index); //pass the car object through intents
        startActivity(intent);
    }

    public void addNewCar(View view) {

        Intent intent = new Intent(this, HomeActivity.class);
        intent.putExtra("newcar", true);
        startActivity(intent);
    }




}


