package com.teamtoast.parking_aid;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.maps.model.LatLng;
import com.team_toast.framework.Location.AndroidGpsLocation;
import com.team_toast.framework.Location.GpsLocation;
import com.team_toast.framework.Location.Location;
import com.team_toast.framework.AppManagement.Notification;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("serial")
public class Car implements Serializable {

    private String regNumber = null;
    private Date timeOfTicket = null;
    private Date timeOfTicketExpiry = null;
    private Location location = null;

    //Notification vars
    private boolean notification15 = false;
    private boolean notification0 = false;

    public Car() {}
    public Car(String regNumber, Date timeOfTicket, Date timeOfTicketExpiry, Location location){

        this.regNumber = regNumber;
        this.timeOfTicket = timeOfTicket;
        this.timeOfTicketExpiry = timeOfTicketExpiry;
        this.location = location;
    }
    public Car(String regNumber, long timeOfTicket, long timeOfTicketExpiry, double lat, double lng, Context context, boolean notification0, boolean notification15){
        Date timeOfTicketDate = new Date();
        timeOfTicketDate.setTime(timeOfTicket);
        Date timeOfTicketExpiryDate = new Date();
        timeOfTicketExpiryDate.setTime(timeOfTicketExpiry);

        this.regNumber = regNumber;
        this.timeOfTicket = timeOfTicketDate;
        this.timeOfTicketExpiry = timeOfTicketExpiryDate;
        this.location = new AndroidGpsLocation(context, new LatLng(lat,lng));

        this.notification0=notification0;
        this.notification15=notification15;
    }

    //Getters
    public String getRegNumber(){ return this.regNumber; }

    public Date getTimeOfTicket(){ return this.timeOfTicket; }

    public Date getTimeOfTicketExpiry(){ return this.timeOfTicketExpiry; }

    public Location getLocation() { return location; }

    public boolean getNotify15(){
        return notification15;
    }

    public boolean getNotify0(){
        return notification0;
    }

    //Setters
    public void setRegNumber(String regNumber){ this.regNumber = regNumber; }

    public void setTimeOfTicket(Date timeOfTicket){ this.timeOfTicket = timeOfTicket; }

    public void setTimeOfTicketExpiry(Date timeOfTicketExpiry){ this.timeOfTicketExpiry = timeOfTicketExpiry; }

    public void setLocation(Location location) { this.location = location; }


    //Methods
    public float getRemainingTicketTime() {

        Date date = new Date();
        return timeOfTicketExpiry.getTime() - date.getTime();
    }

    public String getRemainingTicketTimeStr() {

        Date now = new Date();
        int minsTemp = (int)((timeOfTicketExpiry.getTime()/60000) - (now.getTime()/60000));

        int hours = minsTemp / 60;
        int mins = minsTemp % 60;

        if(hours < 10) {
            if (mins < 10)
                return "0" + hours + " : 0" + mins;
            return "0" + hours + " : " + mins;
        }
        else {
            if (mins < 10)
                return hours + " : 0" + mins;
            return hours + " : " + mins;
        }
    }

    /**
     *  Sends a notification to the device when the parking ticket expires in 15 minutes
     * @param context
     * @param index
     */
    public void sendNotification15(Context context, int index) {

        //Checks if notification has already been sent
        if(!notification15) {
            //Initialise new notification
            Notification n = new Notification();

            //Create notification intent
            Intent intent = new Intent(context, carInfo.class);
            intent.putExtra("carIndex", index);

            //Push notification
            n.pushNotification(
                    context,
                    R.drawable.common_signin_btn_icon_dark,
                    "Parking Reminder!",
                    "Your car " + Assets.cars.get(index).getRegNumber() + " has 15 minutes left",
                    intent);

            //Notification sent, set to true
            notification15 = true;
        }
    }

    /**
     * Sends a notification to the device when the parking ticket has expired
     * @param context
     * @param index
     */
    public void sendNotification0(Context context, int index) {

        //Checks if notification has already been sent
        if(!notification0) {
            //Initialise new notification
            Notification n = new Notification();

            //Create notification intent
            Intent intent = new Intent(context, carInfo.class);
            intent.putExtra("carIndex", index);

            //Push notification
            n.pushNotification(
                    context,
                    R.drawable.common_signin_btn_icon_dark,
                    "Parking Ticket Expired!",
                    "Your car " + Assets.cars.get(index).getRegNumber() + " has expired",
                    intent);

            //Notification sent, set to true
            notification0 = true;
        }
    }

    public String toString(){
        return "Reg Number: " + this.regNumber + "\nTime left: " + this.getRemainingTicketTimeStr();}
}
